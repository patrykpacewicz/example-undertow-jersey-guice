package pl.patrykpacewicz;

import org.glassfish.jersey.server.ResourceConfig;

public class DeployApplication extends ResourceConfig {
    public DeployApplication() { packages("pl.patrykpacewicz.resource"); }
}
