package pl.patrykpacewicz.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/")
public class HelloWorldResource {
    @GET
    public String returnHelloWorld() {
        return "Hello World!";
    }
}
