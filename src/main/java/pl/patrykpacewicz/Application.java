package pl.patrykpacewicz;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;

public class Application {
    public static void main(String[] args) {
        new UndertowJaxrsServer().deploy(DeployApplication.class).start();
    }
}
