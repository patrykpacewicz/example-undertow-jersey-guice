class example::params {
    $cwd       = $::apppath
    $buildPath = "build/libs"
    $appName   = "example-undertow-jersey-guice"
    $file      = "$appName.jar"
    $path      = "$cwd/$buildPath/$file"
}
