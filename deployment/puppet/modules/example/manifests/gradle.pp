class example::gradle(
  $command = "clean build",
) inherits example::params {
    exec {'gradlew':
        command => "gradlew $command",
        cwd => $cwd,
        path => [$cwd, '/bin', '/usr/bin'],
        unless => "test -f $path"
    }
}
