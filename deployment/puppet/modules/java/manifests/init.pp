class java (
    $version = $java::params::version,
    $ppa     = $java::params::ppa
) inherits java::params {
    include apt

    apt::ppa { $ppa: }

    exec { 'set-licence-selected':
        command => 'echo debconf shared/accepted-oracle-license-v1-1 select true |debconf-set-selections',
        path => ['/bin', '/usr/bin/'],
        unless => "dpkg -l | grep $version"
    }

    exec { 'set-licence-seen':
        command => 'echo debconf shared/accepted-oracle-license-v1-1 seen true |debconf-set-selections',
        path => ['/bin', '/usr/bin/'],
        unless => "dpkg -l | grep $version"
    }

    package { $version:
        ensure => installed,
        require => [
            Apt::Ppa[$ppa],
            Exec['set-licence-selected'],
            Exec['set-licence-seen']
        ]
    }
}