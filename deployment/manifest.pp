class { 'supervisord': install_pip => true }
class { 'java': version => 'oracle-java8-installer' }

class { 'example':
    require => [ Class['supervisord'], Class['java'] ]
}

supervisord::program { 'example':
    command => "java -jar $example::params::path",
    require => Class['example']
}
