#!/bin/bash

logFile=/var/log/deployment-bootstrap.log

function logger_info () {  echo $(date +"%F %X") [INFO ] $@ |tee -a $logFile; }
function logger_error () { echo $(date +"%F %X") [ERROR] $@ |tee -a $logFile; exit 255; }
function command () { $@ 2>&1 |tee -a $logFile; [ $PIPESTATUS -eq 0 ] || logger_error $@; }

function puppet_module_install () { puppet module list |grep $1 || puppet module install $1; }
function puppet_install () {
    ubuntuCodeName=$(lsb_release -c | awk '{print $2}')
    puppetFile=puppetlabs-release-${ubuntuCodeName}.deb

    if [ ! -e /tmp/${puppetFile} ]; then
        command wget -q http://apt.puppetlabs.com/${puppetFile} -O /tmp/${puppetFile}
        command dpkg -i /tmp/${puppetFile}
        command apt-get -qqy update
        command apt-get -qqy install puppet
    fi
}

logger_info Checking if user is root
command [ $EUID -eq 0 ]

logger_info Install puppet
command puppet_install

logger_info Install puppet modules
command puppet_module_install ajcrowe-supervisord
command puppet_module_install puppetlabs-apt

logger_info Run application installation
applicationPath=${applicationPath:='/vagrant'}

export FACTER_appPath=$applicationPath

command puppet apply \
        --modulepath=$applicationPath/deployment/puppet/modules:/etc/puppet/modules \
        --verbose $applicationPath/deployment/manifest.pp
